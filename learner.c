//
// OpenCL definitions
#ifndef SORT_H
#define SORT_H


#ifdef __APPLE__
#include <OpenCL/opencl.h>
#else
#include <CL/cl.h>
#endif

#ifndef __has_extension
#define __has_extension __has_feature // Compatibility with pre-3.0 compilers.
#endif
#ifndef __has_feature      // Optional of course.
#define __has_feature(x) 0 // Compatibility with non-clang compilers.
#endif
#ifndef __has_builtin      // Optional of course.
#define __has_builtin(x) 0 // Compatibility with non-clang compilers.
#endif

typedef unsigned int uint;

#if __has_builtin(__builtin_shufflevector)
#define shuffle __builtin_shufflevector
#endif
#if defined(_MSC_VER)
/* Microsoft C/C++-compatible compiler */
#include <intrin.h>
#elif defined(__GNUC__) && (defined(__x86_64__) || defined(__i386__))
/* GCC-compatible compiler, targeting x86/x86-64 */
#include <x86intrin.h>
#elif defined(__GNUC__) && defined(__ARM_NEON__)
/* GCC-compatible compiler, targeting ARM with NEON */
#include <arm_neon.h>
#elif defined(__GNUC__) && defined(__IWMMXT__)
/* GCC-compatible compiler, targeting ARM with WMMX */
#include <mmintrin.h>
#elif (defined(__GNUC__) || defined(__xlC__)) &&                               \
    (defined(__VEC__) || defined(__ALTIVEC__))
/* XLC or GCC-compatible compiler, targeting PowerPC with VMX/VSX */
#include <altivec.h>
#elif defined(__GNUC__) && defined(__SPE__)
/* GCC-compatible compiler, targeting PowerPC with SPE */
#include <spe.h>
#endif

#if __has_feature(attribute_ext_vector_type)
// This code will only be compiled with the -std=c++11 and -std=gnu++11
// options, because rvalue references are only standardized in C++11.
typedef cl_uchar uint8_t;
typedef cl_ushort uint16_t;
typedef cl_uint uint32_t;
typedef cl_ulong uint64_t;
typedef cl_short4 v4si;
typedef cl_uchar16 v16uc;
// typedef cl_ushort16 v16us;
typedef unsigned int v16us __attribute__((ext_vector_type(16)));
typedef cl_ushort8 v8us;
typedef unsigned int v4us __attribute__((ext_vector_type(4)));
typedef unsigned char v2uc __attribute__((ext_vector_type(2)));
// typedef cl_ushort4 v4us;
#else
typedef cl_uchar uint8_t;
typedef cl_uchar2 v2uc;
typedef cl_ushort uint16_t;
typedef cl_uint uint32_t;
typedef cl_ulong uint64_t;
typedef cl_short4 v4si;
typedef cl_uchar16 v16uc;
typedef cl_ushort16 v16us;
typedef cl_ushort8 v8us;
typedef cl_ushort4 v4us;
#endif

#define V8US_LONG 16
/*#define NULL 0*/


#define TRUE 1
#define FALSE 0

#endif
// series specification _acc required _rea
// slispfifka pyicli
#include <string.h>
// boolean specification _acc required _rea
// bl67npfifka pyicli
#include <stdbool.h>
// integer specification _acc required _rea
// tyuspfifka pyicli
#include <stdint.h>
// zero agent itinerary specification _acc required _rea
// zronnwus jricka pyicli
#include <zmq.h>
// 1yon _nom zero agent itinerary _acc denote _rea
// 1yonna zronnwus jricka dlutli

void gather(const char teaching, const char answer,
            uint16_t *trainingCollectorLong, v2uc * trainingCollector) {
  trainingCollector[*trainingCollectorLong].s0 = teaching;
  trainingCollector[*trainingCollectorLong].s1 = answer;
  *trainingCollectorLong += 1;
}
void printTrainingCollector(const uint16_t trainingCollectorLong,
                            v2uc *trainingCollector) {
  uint16_t trainingCollector_indexFinger = 0;
  for (; trainingCollector_indexFinger < trainingCollectorLong;
       ++trainingCollector_indexFinger) {
    printf("%c ",trainingCollector[trainingCollector_indexFinger].s0);
    printf("%c\n",trainingCollector[trainingCollector_indexFinger].s1);
  }
}
// cardinal _top begin _rea
int main() {
  // This is an example of a silly learner that always replies with '.'
  // answer _nom one _allative_case letter _rea
  // hnapna hyikdolweh lyatli
  char answer[1];
  char reward[1];
  char teaching[1];
  uint16_t trainingCollectorLong = 0;
  v2uc trainingCollector[0x10] = {0};

  // number _nom zero _num _acc integer plural_number _rea
  // hnucna zrondoka tyushpacli
  int n = 0;
  // alphabet _nom _quoted.letter.00101110.letter._quoted _acc
  //   preordained letter referential _rea
  // lwamna zi.prih.00101110.prih.zika pwutlyathrecli
  const char *alphabet = " abcdefghijklmnopqrstuvwxyz1234567890ABCDEFGHIJKLMNOP"
                         "QRSTUVWXYZ,.!;?-"; // '.' utf-8 code

  // connect
  // scene _nom hollow referential _rea
  // tfacna hcochrecli
  // scene _dat zero agent itinerary scene establish _deo
  // tfacyi 1yontliptu
  void *context = zmq_ctx_new();
  // ecology _nom hollow referential _rea
  // qlitna hcochrecli
  // ecology _dat scene _acc 1yon mate _ins 1yon plug _deo
  // qlityi 1yondyatyu tfacka 1yontlektu
  void *to_env = zmq_socket(context, ZMQ_PAIR);
  // return code _nom integer plural_number _rea
  // hwithkomna tyushpacli
  // return code _dat ecology _acc
  //    _quoted.letter.tcp://localhost:5556.letter._quoted _ins 1yon
  //    attaching _deo
  // hwithkomyi qlitka zi.prih.tcp://localhost:5556.prih.ziyu 1yondxantu
  int rc = zmq_connect(to_env, "tcp://localhost:5556");

  // handshake
  // ecology _loc zero _num _ins five _num _allative_case
  //   _quoted.letter.hello.letter._quoted _acc 1yon sent _deo
  // qlitte zrondoyu  hfakdolweh zi.prih.hello.prih.zika 1yonhfattu
  zmq_send(to_env, "hello", 5, 0);

  // talk
  // index_finger _nom zero _num _acc natural number _rea
  uint16_t indexFinger = 0;
  // cyclic _top index_finger _acc one _num _ins
  //   fifteen fifteen fifteen fifteen _num _allative_case begin _rea
  for (; indexFinger < 0xFF; ++indexFinger) {
    // receive reward
    // ecology _abl answer _dat one _num _allative_case 1yon accept _deo
    // qlitpwih hnapyi hyikdolweh ksaptu
    zmq_recv(to_env, reward, 1, 0);
    printf("%c reward\n", *reward);

    if (reward[0] == '1') {
      gather(teaching[0], answer[0], &trainingCollectorLong, trainingCollector);
    }

    printf("0x%X moment, ", indexFinger);
    // receive teacher/env bit
    // ecology _abl answer _dat one _num _allative_case 1yon accept _deo
    // qlitpwih hnapyi hyikdolweh ksaptu
    zmq_recv(to_env, teaching, 1, 0);
    printf("%c teaching, ", *teaching);
    // answer
    answer[0] = alphabet[n % strlen(alphabet)];
    printf("%c answer, ", *answer);
    zmq_send(to_env, answer, 1, 0);
    n += 1;
  }
  printf("\ntraining collection:\n");
  printTrainingCollector(trainingCollectorLong, trainingCollector);

  zmq_close(to_env);
  zmq_ctx_destroy(context);

  return 0;
}
