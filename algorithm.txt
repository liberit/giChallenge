
for micro challenge, 
output random characters till get a reward,
once have two or three rewards, 
then can evolve a program that goes from the input to the output, with the first
two samples, 
and test it with the third. 
can then try it on the next byte,
if it is right, then keep going,

if it is wrong, go back to random until get new reward,
and evolve another program. 

Can also continue evolution in background, adding the new training and
verification samples to the islands. 
Since we're doing it on GPU there is no point stopping the evolution at any
point, best to simply keep going.  Can simply spawn new islands and such, and
new populations, depending on what's available. 

The champion programs will be saved, and useable by future programs. 

when on the feedback challenge will be easier, as don't have to do random
guesses, since the feedback will be given. 

task-switching detection would be necessary, as may need to make new populations
if that is the case, and use only the new rewards. 

If it is right for a while, and then wrong several times, 
that is a task switch and have to reset the populations. 


