#!/bin/bash
clang learner.c -lzmq -o Round1/learner || exit
cd Round1/src || exit
python run.py tasks_config.json  -l learners.base.RemoteLearner \
--learner-cmd "../learner"
